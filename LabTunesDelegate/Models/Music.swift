//
//  Music.swift
//  LabTunesDelegate
//
//  Created by markmota on 11/21/18.
//  Copyright © 2018 markmota. All rights reserved.
//

import Foundation
class Music {
    static var urlSession = URLSession(configuration: .default)
    static func fetchSongs(songName: String = "TheBeatles"){
         guard let url = URL(string: "https://itunes.apple.com/search/media=music&identity=song&term=\(songName))") else {return}
        
        let dataTask = urlSession.dataTask(with: url){ data, response, error in
            if let error = error {
                debugPrint("Error in dataTask: \(error.localizedDescription)")
                return
            }
            guard let httpResponse = response as? HTTPURLResponse, (200 ... 299).contains(httpResponse.statusCode) else{
                debugPrint("Error in httpResponse, code out of range between 200 ... 299")
                return
                
            }
            guard let data = data, let songsList = try?JSONDecoder().decode(Songs.self, from: data) else{return}
            debugPrint(songsList)
        }
        dataTask.resume()
    }
}

